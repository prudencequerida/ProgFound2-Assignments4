import javax.sound.sampled.*;

public class Sound {

    private Clip clip;
    public static Sound bgm = new Sound("music/star-wars-theme-song.wav");
    public static Sound benar = new Sound("music/toot.wav");

    /**
     * Constructor of the Sound class
     * @param filename
     */
    public Sound(String filename) {
        try {
            AudioInputStream ais = AudioSystem.getAudioInputStream(Sound.class.getResource(filename));
            clip = AudioSystem.getClip();
            clip.open(ais);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Accessor of clip variable
     * @return
     */
    public Clip getClip() {
        return clip;
    }

    /**
     * method to play the song
     */
    public void play() {
        try {
            if (clip != null) {
                new Thread() {
                    public void run() {
                        synchronized (clip) {
                            clip.stop();
                            clip.setFramePosition(0);
                            clip.start();
                        }
                    }
                }.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method to loop the song
     */
    public void loop() {
        try {
            if (clip != null) {
                new Thread() {
                    public void run() {
                        synchronized (clip) {
                            clip.stop();
                            clip.setFramePosition(0);
                            clip.loop(Clip.LOOP_CONTINUOUSLY);
                        }
                    }
                }.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}