import javax.swing.JButton;
import javax.swing.ImageIcon;

public class Card {
    private int id;
    private boolean matched = false;
    private ImageIcon pressed;
    private ImageIcon icon;
    private final JButton button = new JButton();

    /**
     * Constructor for the Card class
     * @param id, the images id
     * @param icon, cover of the button
     * @param pressed, image behind the button
     */
    public Card(int id, ImageIcon icon, ImageIcon pressed) {
        this.id = id;
        this.matched = matched;
        this.icon = icon;
        this.button.setIcon(icon);
        this.pressed = pressed;
    }

    /**
     * Accessor to get id of a card
     * @return
     */
    public int getId() {

        return id;
    }

    public JButton getButton() {


        return button;
    }

    public void pressed() {


        button.setIcon(pressed);
    }

    public void release() {


        button.setIcon(icon);
    }

    public boolean getMatched() {


        return this.matched;
    }

    public void setMatched(boolean matched) {


        this.matched = matched;
    }
}