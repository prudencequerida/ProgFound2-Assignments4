// Prudence Querida
// 16606863503

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.*;

class Board extends JFrame {

    /**
     * Instance variables
     */
    private List<Card> cards;
    private Card selectedCard;
    private Card c1;
    private Card c2;
    private Timer t;
    private Timer wait;
    private Timer openTime;
    private int reset = 0;
    private int tries = 0;
    private JFrame mainframe;
    private JLabel labelTries;
    private JLabel labelReset;
    private JPanel top;
    private JButton buttonReset;
    private JButton buttonExit;
    private int reveal = 0;

    /**
     * Constructor of Board
     */
    public Board() {
        int pairs = 18;
        List<Card> cardsList = new ArrayList<>();
        List<Integer> cardVals = new ArrayList<>();

        for (int i = 0; i < pairs; i++) {
            cardVals.add(i);
            cardVals.add(i);
        }
        Collections.shuffle(cardVals);

        for (int val : cardVals) {
            String[] chars = {
                    "1", "2", "3", "4", "5",
                    "6", "7", "8", "9", "10",
                    "11", "12", "13", "14", "15",
                    "16", "17", "18"};
            String path = "/Users/prudencequerida/Documents/KULIAH/SEMESTER-4/DDP2/TP-4/ProgFound2-Assignments4/src/main/java/assets/";
            Card c = new Card(val, resizeIcon(new ImageIcon(path + "logo.png")), resizeIcon(new ImageIcon(path + chars[val] + ".png")));
            c.getButton().addActionListener(e -> doTurn(c));
            cardsList.add(c);
        }

        this.cards = cardsList;

        t = new Timer(750, e -> checkCards());
        t.setRepeats(false);

        createPanel();
    }

    /**
     * method to create game panel
     */
    public void createPanel() {
        mainframe = new JFrame("Match-Pair Memory Game");
        mainframe.setLayout(new BorderLayout());
        top = new JPanel();
        top.setLayout(new GridLayout(6, 6));
        for (Card c : cards) {
            top.add(c.getButton());
        }
        JPanel bottom = new JPanel(new GridLayout(2, 2));
        JButton buttonReset = new JButton("Reset");
        JButton buttonExit = new JButton("Exit");
        bottom.add(buttonReset);
        bottom.add(buttonExit);

        labelReset = new JLabel("Number of Reset: " + reset);
        labelTries = new JLabel("Number of Tries: " + tries);
        labelTries.setHorizontalAlignment(SwingConstants.CENTER);
        labelReset.setHorizontalAlignment(SwingConstants.CENTER);
        bottom.add(labelTries);
        bottom.add(labelReset);
        buttonReset.addActionListener(actionPerformed -> reset(cards));
        buttonExit.addActionListener(actionPerformed -> exit());

        mainframe.add(top, BorderLayout.CENTER);
        mainframe.add(bottom, BorderLayout.SOUTH);
        mainframe.setPreferredSize(new Dimension(800, 800));
        mainframe.setLocation(500, 250);
        mainframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainframe.pack();
        mainframe.setVisible(true);
        wait = new Timer(2000, actionPerformed -> revealAllCards());
        wait.start();
        wait.setRepeats(false);
    }

    /**
     * method to implement the exit button
     */
    private void exit() {
        System.exit(0);
    }

    /**
     * method to implement the reset button
     * @param cards, all the cards that will be reset
     */
    private void reset(List<Card> cards) {
        mainframe.remove(top);
        Collections.shuffle(cards);
        JPanel top = new JPanel(new GridLayout(6, 6));
        for (Card card : cards) {
            card.setMatched(false);
            card.release();
            card.getButton().setEnabled(true);
            top.add(card.getButton());
        }
        mainframe.add(top, BorderLayout.CENTER);
        reset++;
        tries = 0;
        labelReset.setText("Number of Resets: " + reset);
        labelTries.setText("Number of Tries: " + tries);
        revealAllCards();
    }

    /**
     * method to reveal all the cards during the start of the game
     */
    public void revealAllCards() {
        for (Card card : cards) {
            card.pressed();
        }
        top.updateUI();
        openTime = new Timer(2000, actionPerformed -> hideAllCards());
        openTime.setRepeats(false);
        openTime.start();
    }

    /**
     * method to hide all the cards again after reveal at the start of the game
     */

    public void hideAllCards() {
        for (Card card : cards) {
            card.release();
        }
        top.updateUI();
    }

    /**
     * method to open up the selected card
     * @param selectedCard, card you selected
     */
    public void doTurn(Card selectedCard) {
        if (c1 == null && c2 == null) {
            c1 = selectedCard;
            c1.pressed();
        }

        if (c1 != null && c1 != selectedCard && c2 == null) {
            c2 = selectedCard;
            c2.pressed();
            t.start();
        }
    }

    /**
     * method to disable and flags button if the id matched.
     */
    public void matchButton() {
        c1.getButton().setEnabled(false);
        c2.getButton().setEnabled(false);
        c1.setMatched(true);
        c2.setMatched(true);
    }

    /**
     * method to check if both cards selected is a match or not
     */
    public void checkCards() {
        if (c1.getId() == c2.getId()) { //match condition
            Sound.benar.play();
            c1.getButton().setEnabled(false); //disables the button
            c2.getButton().setEnabled(false);
            c1.setMatched(true); //flags the button as having been matched
            c2.setMatched(true);
            if (this.isGameWon()) {
                JOptionPane.showMessageDialog(this, "You win!\nNumber of "
                        + "tries: " + tries + "\nNumber of Reset: " + reset);
                System.exit(0);
            }
        } else {
            c1.release();
            c2.release();
            tries++;
            labelTries.setText("Number of Tries: " + tries);
        }
        c1 = null; //reset c1 and c2
        c2 = null;
    }

    /**
     * method to check if the game is done or not
     * @return true if done, and vice versa
     */
    public boolean isGameWon() {
        for (Card c : this.cards) {
            if (!c.getMatched()) {
                return false;
            }
        }
        return true;
    }

    /**
     * method to resize the image to fit the card
     * @param icon
     * @return the resized image
     */
    public static ImageIcon resizeIcon(ImageIcon icon) {
        Image img = icon.getImage();
        Image resizedImage = img.getScaledInstance(135, 120, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }
}